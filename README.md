The Puc format is an XML compressed format. Proposed by the ANR Project Kinsources Team, this data model was developed to be able to represent rich kinship information in a dataset. It is hosted on the GitLab repository manager: https://gitlab.com/kinsources/Puc_format

The XML format has been choosen because of its simplicity, openess, flexibility, extensibility, and durability. Indeed, XML was created about 20 years ago (XML 1.0 in 1998) and files of the '90s are still perfectly readable by a large scale of software, whatever the operating system (Windows, Mac, Linux, Unix...). Moreover, the XML format is widely used, supported by the W3C, and almost all programming languages include libraries, packages, or special functions for XML managment (create, read, modify, look into...).

The Puc format can encode very large dataset (no size limit), thus we choose to compress the XML file in a zip archive (about 10 times lighter). So a .puc file can be open with whatever archive manager: the uncompress file is the XML file.

An XML Schema Definition, XML example file, and perl validation script are available ([here](https://gitlab.com/kinsources/Puc_format/tree/master)).

---


**Useful links:**
  * Direct access to files: https://gitlab.com/kinsources/Puc_format/tree/master
  * Puck software repository: https://sourceforge.net/projects/tip-puck/
  * Kinsources.net website : https://www.kinsources.net/