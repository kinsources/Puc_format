#!/usr/bin/perl
use strict;
use warnings;

use XML::LibXML;
use XML::Parser;
use XML::SAX::ParserFactory;
use XML::Validator::Schema;

if (not($#ARGV==1)) {
    print "\n   syntaxe : testevsp fichier[.xml] schema[.xsd] \n\n" ;
    exit(-1) ;
} ; # fin de si

my $FichierXML = $ARGV[0] ;
my $FichierXSD = $ARGV[1] ;

print "Fichier : \"$FichierXML\"\n";

## Validation XML du fichier : on vérifie qu'il est bien formé
#my $parser= new XML::Parser(); # Style => 'Stream'); #si on veut afficher le fichier en sortie
#eval {$parser->parsefile( $FichierXML )};
my $parser     = XML::LibXML->new();
# Activation validation DTD du fichier XML avant le parsing
$parser->validation(0);
# Validation à proprement parlée
eval { $parser->parse_file($FichierXML)};
die "  Ce document XML est mal formé :\n $@ \n" if $@;
print "  Ce document XML est bien formé.\n" ;


# Lecture et récupération de la racine du document XML
my $tree = $parser->parse_file($FichierXML);
my $root = $tree->getDocumentElement;

# Validation XSD du fichier XML
## Récupérons le fichier XSD dans la balise annuaire
#my $FichierXSD = $root->getAttribute('xsi:noNamespaceSchemaLocation');
my $schema = XML::LibXML::Schema->new( location => $FichierXSD );

eval { $schema->validate($tree) };
die "  [XSD] Ce document XML est non valide au schéma XSD \"$FichierXSD\" :\n    $@" if $@;
print "  Ce document XML est valide au schéma XSD \"$FichierXSD\".\n" ;

